#!/bin/sh
green=`tput setaf 2`
red=`tput setaf 1`
blue=`tput setaf 4`

clear

read -e -p "siteurl: " -i "blupal.test" siteUrl
read -e -p "database username: " -i "root" dbUsername
read -e -p "database password: " -i "root" dbPassword

IFS='.' read -r -a array <<< "$siteUrl"

read -e -p "database name: " -i "${array[0]}" dbName
read -e -p "clear blupal git informations: " -i "no" clearGit

#Composer update
composer update

printf 'y' | drush site-install standard --db-url='mysql://'$dbUsername':'$dbPassword'@localhost/'$dbName --site-name=Blupal --account-name=admin --account-pass=12345677
echo "${green} - Drupal installed by drush"

sudo chmod 0777 config/sync
echo "Write right set to config/sync"

mkdir content
mkdir content/sync
sudo chmod 0777 content/sync
echo "Create and write right set to content/sync"

# Configure services
echo 
sudo chmod 0777 web/sites/default
sudo sh -c "echo '' > web/sites/default/services.yml"
sudo chmod 0777 web/sites/default/services.yml
drupal site:mode dev
sudo chmod 0644 web/sites/default/services.yml
sudo chmod 0755 web/sites/default
echo " - ${green}Blupal set in mode dev"
echo
# Configure settings
echo 
result_string="${siteUrl/./\\.}"
echo "${green}trusted_host_patterns set in web/sites/default/settings.php"
trusted_line="\$settings['trusted_host_patterns'] = array('^"$result_string"\$');"

sudo chmod 0777 web/sites/default/settings.php
cat >> web/sites/default/settings.php <<EOF

$trusted_line
global \$content_directories;
\$content_directories['sync'] = \$app_root.'/../content/sync';
EOF



# set the same UUID than Blupal git
printf 'y' | drush config-set "system.site" uuid 'f635b51d-d10d-4839-8c63-e3a41656141e'
echo "UUID modified"

# delete shortcuts (to avoid a bug with config import)
drush ev '\Drupal::entityManager()->getStorage("shortcut_set")->load("default")->delete();'
echo "shortcuts deleted"

drush sql-query "DELETE FROM key_value WHERE collection='system.schema' AND name='module_name';"
#import configuration from sync files
printf 'y' | drush config-import
echo "configuration imported"

drush pmu shortcut -y;
drush en shortcut -y;



cp web/sites/example.settings.local.php web/sites/default/settings.local.php
cat >> web/sites/default/settings.php <<EOF

if (file_exists(__DIR__ . '/settings.local.php')) {
  include __DIR__ . '/settings.local.php';
}
EOF
echo "${green}settings.local.php installed"

cp web/sites/example.settings.local.php web/sites/default/settings.local.php


sed -i '/ $settings.*cache.*bins.*render.*/s/^#//g' web/sites/default/settings.local.php
sed -i '/ $settings.*cache.*bins.*dynamic_page_cache.*/s/^#//g' web/sites/default/settings.local.php
sed -i '/.*$settings.*rebuild.*access.*/c\$settings["rebuild_access"] = FALSE;' web/sites/default/settings.local.php;


cat >> web/sites/development.services.yml <<EOF
parameters:
  twig.config:
    debug : true
    auto_reload: true
    cache: false
EOF
echo "${green}Cache disabled"
drush cr


sudo chmod 0444 web/sites/default/settings.php
echo "${green} - web/sites/default/settings.php chmod is now 0444"

sudo chmod 0444 web/sites/default/settings.local.php
echo "${green} - web/sites/default/seettings.local.php chmod is now 0444"

sudo chmod 0444 web/sites/default/services.yml
echo "${green} - web/sites/default/services.yml chmod is now 0444"

echo 

drush sql-query "UPDATE users_field_data SET mail='admin@bluesystem.ch' where uid=1"
drush sql-query "DELETE FROM users WHERE uid = '0';"

echo "${green} login is admin@bluesystem.ch - 12345677"